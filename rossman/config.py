#!/usr/bin/env python
# coding: utf8

import os
SQLITE_DB_NAME = 'rossman.db'
POSTGRE_DB_NAME = 'rossman'
POSTGRE_USR = 'postgres'
POSTGRE_PWD = '123456'
POSTGRE_HOST = 'localhost'
POSTGRE_PORT = '5432'
FACTORS_FILE_NAME = 'factors.json'