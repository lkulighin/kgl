#!/usr/bin/env python
# coding: utf8
"""
Initial OLS model
"""
import psycopg2
import pandas as pd
import numpy as np
import traceback
#from sklearn import linear_model
import statsmodels.api as sm

from config import POSTGRE_DB_NAME, POSTGRE_USR, POSTGRE_PWD, POSTGRE_HOST, POSTGRE_PORT
features_list = ['dow', 'month_', 'week_', 'month_', 'f1', 'f2', 'f3']
features_list1 = ['dow', 'month_', 'week_', 'month_', 'f1', 'f2', 'f3', 'ast_b', 'ast_c', 'intercept']

def model1(conn):
    try:
        df = pd.read_sql_query('select sales, date_part(\'DOW\',dayref) dow, date_part(\'MONTH\',dayref) month_, week_, assortment, f1, f2, f3 \
            from TrainData INNER JOIN Stores ON TrainData.Store = Stores.Store WHERE year_ = >= 2014 ', con=conn)
        features = df[features_list]
        features['intercept'] = 1.0
        target = df['sales']

        #create dummy variables for categorical variables
        dummy_ranks = pd.get_dummies(df['assortment'], prefix='ast')
        features = features.join(dummy_ranks.ix[:, 'ast_b':])
        #print features.head(5)

        #reg = linear_model.LinearRegression()
        #reg.fit(features, target)
        reg = sm.OLS(target.astype(float), features, missing='drop')
        model = reg.fit()
        model.save("model1.pickle")
        print(model.summary())
    except:
        print 'error', traceback.format_exc()


if __name__ == "__main__":
    try:
        conn = psycopg2.connect(database=POSTGRE_DB_NAME, user=POSTGRE_USR, password=POSTGRE_PWD, host=POSTGRE_HOST, port=POSTGRE_PORT)
        #model1(conn)
        from statsmodels.iolib.smpickle import load_pickle
        model = load_pickle("model1.pickle")
        print(model.summary())

        #make predictions
        features_test = pd.read_sql_query('select id, t1.store, t1.dayref, date_part(\'DOW\',dayref) dow, date_part(\'MONTH\',dayref) month_, week_, assortment, f1, f2, f3 \
            from TestData t1 INNER JOIN Stores ON t1.Store = Stores.Store', con=conn)
        #features_test = df_test[features_list]
        features_test['intercept'] = 1.0
        dummy_ranks = pd.get_dummies(features_test['assortment'], prefix='ast')
        features_test = features_test.join(dummy_ranks.ix[:, 'ast_b':])

        print features_test.head(5)
        features_test['sales_pred'] = model.predict(features_test[features_list1])


        res = features_test[['id', 'sales_pred']]
        res.to_csv('subm.csv', sep=',')

    except:
        print 'error', traceback.format_exc()
