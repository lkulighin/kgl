SELECT date_part('YEAR', dayref)
, date_part('WEEK', dayref)
, COUNT(DISTINCT dayref)
FROM TrainData
GROUP BY date_part('YEAR', dayref), date_part('WEEK', dayref)
ORDER BY 1, 2

--f1 = avg weekly sales LY
UPDATE TrainData t1
SET f1 = sw.sales
FROM salesweekly sw
WHERE sw.Store = t1.Store AND sw.week = t1.week_
AND sw.year = t1.year_ - 1

UPDATE $tablename$ t1
SET f1 = sw.sales
FROM stores st, avgsalesweekly sw
WHERE sw.week_ = t1.week_ AND sw.year_ = t1.year_ - 1
AND sw.asst = st.assortment
AND st.store = t1.store
AND t1.f1 IS NULL


--f2 = avg weekly sales LY-1
UPDATE TrainData t1
SET f2 = sw.sales
FROM salesweekly sw
WHERE sw.Store = t1.Store
AND t1.week_+(case when t1.week_-1 > 0 then -1 else 51 end) = sw.week
AND t1.year_+(case when t1.week_-1 > 0 then -1 else -2 end) = sw.year

UPDATE $tablename$ t1
SET f2 = sw.sales
FROM stores st, avgsalesweekly sw
WHERE sw.asst = st.assortment
AND t1.week_+(case when t1.week_-1 > 0 then -1 else 51 end) = sw.week_
AND t1.year_+(case when t1.week_-1 > 0 then -1 else -2 end) = sw.year_
AND st.store = t1.store
AND t1.f2 IS NULL

--f3 = avg weekly sales LY - 2
UPDATE TrainData
SET f3 = sw.sales
FROM salesweekly sw
WHERE sw.Store = TrainData.Store
AND TrainData.week_+(case when TrainData.week_-2 > 0 then -2 else 50 end) = sw.week
AND TrainData.year_+(case when TrainData.week_-2 > 0 then -1 else -2 end) = sw.year

UPDATE $tablename$ t1
SET f3 = sw.sales
FROM stores st, avgsalesweekly sw
WHERE sw.asst = st.assortment
AND t1.week_+(case when t1.week_-2 > 0 then -2 else 50 end) = sw.week_
AND t1.year_+(case when t1.week_-2 > 0 then -1 else -2 end) = sw.year_
AND st.store = t1.store
AND t1.f3 IS NULL


--f4 = avg weekly sales LY - 3
UPDATE TrainData
SET f4 = sw.sales
FROM salesweekly sw
WHERE sw.Store = TrainData.Store
AND TrainData.week_+(case when TrainData.week_-2 > 0 then -2 else 50 end) = sw.week
AND TrainData.year_+(case when TrainData.week_-2 > 0 then -1 else -2 end) = sw.year


f1 = avg sales day of week -1
avg sales day of week -2
avg sales day of week -3
avg sales day of week -4
