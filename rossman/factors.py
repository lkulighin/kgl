#!/usr/bin/env python
# coding: utf8
"""
Factors calculation and prediction
"""

import traceback
import psycopg2

from config import POSTGRE_DB_NAME, POSTGRE_USR, POSTGRE_PWD, POSTGRE_HOST, POSTGRE_PORT

class Factor:
    def __init__(self, **kwargs):
        self.name = kwargs['name']
        if 'description' in kwargs:
            self.description = kwargs['description']
        if 'steps' in kwargs:
            self.steps = int(kwargs['steps'])
        else:
            self.steps = None

        if 'sql_query' in kwargs:
            self.sql_query = []
            queries = kwargs['sql_query']
            if self.steps <> len(queries):
                raise ValueError("Wrong amount of steps for factor " + self.name)
            for q in queries:
                self.sql_query.append(queries[q])
        if 'author' in kwargs:
            self.author = kwargs['author']

class Factors:
    def __init__(self):
        self.factors = []
        from config import FACTORS_FILE_NAME

        #read the config file
        import json
        try:
            with open(FACTORS_FILE_NAME) as data_file:
                data = json.load(data_file)
        except:
            raise ValueError("JSON with factors description not found")

        #create a list of factors
        data = data['factorslist']
        for el in data:
            try:
                factor = Factor(**el)
            except:
                print 'error', traceback.format_exc()
                raise ValueError("Error in initialization the factor " + el['name'])
            self.factors.append(factor)

    def getfactor(self, name):
        return self.getfactors(dictionary = True)[name]

    def getfactors(self, **kwargs):
        if 'dictionary' in kwargs:
            if kwargs['dictionary'] == True:
                return {f.name: f for f in self.factors}
            else:
                return self.factors
        else:
            return self.factors

    def getfactorsnames(self):
        return [f.name for f in self.factors]

    def calculate(self, conn, tablename, *factors_list):
        factors_list = list(factors_list)
        cur = conn.cursor()
        if len(factors_list) == 0:
            factors_list = self.getfactorsnames()
        try:
            for name in factors_list:
                factor = self.getfactor(name)
                for sql_text in factor.sql_query:
                    sql_text = sql_text.replace('$tablename$', tablename)
                    #print sql_text
                    cur.execute(sql_text)
                    conn.commit()
        except:
            print 'error', traceback.format_exc()
            raise ValueError("Error in calculating factor " + name)

def test():
    try:
        conn = psycopg2.connect(database=POSTGRE_DB_NAME, user=POSTGRE_USR, password=POSTGRE_PWD, host=POSTGRE_HOST, port=POSTGRE_PORT)
        #factors_calculation(1,2,3,4)
        factors = Factors()
        #factors.calculate(conn, 'TrainData', 'f3')
        factors.calculate(conn, 'TrainData')
        factors.calculate(conn, 'TestData')
        conn.close()
    except:
        print 'error', traceback.format_exc()


if __name__ == "__main__":
    test()
