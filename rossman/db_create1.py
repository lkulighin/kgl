#!/usr/bin/env python
# coding: utf8
"""
Create a postgresql db, import and normilize the data
"""
import psycopg2
import os
import traceback
import csv

from config import POSTGRE_DB_NAME, POSTGRE_USR, POSTGRE_PWD, POSTGRE_HOST, POSTGRE_PORT
#paths to files with data
PATH_STORES = 'data\\store.csv'
PATH_TRAIN = 'data\\train.csv'
PATH_TEST = 'data\\test.csv'
#mapping  external fields to internal
FIELDS_TRAIN = {'Store': ['Store', 'int'], 'Date': ['DayRef', 'date'], 'Sales': ['Sales', 'int'], 'Customers': ['Customers', 'int'], 'Open': ['Open', 'int'], \
'Promo': ['Promo', 'int'], 'StateHoliday': ['StateHoliday ', 'text'], 'SchoolHoliday': ['SchoolHoliday', 'int']}
FIELDS_TEST = {'Id': ['ID', 'int'], 'Store': ['Store', 'int'], 'Date': ['DayRef', 'date'], 'Open': ['Open', 'int'], \
'Promo': ['Promo', 'int'], 'StateHoliday': ['StateHoliday ', 'text'], 'SchoolHoliday': ['SchoolHoliday', 'int']}
FIELDS_STORES = {'Store': ['Store', 'int'], 'StoreType': ['Type', 'text'], 'Assortment': ['Assortment', 'text'], 'CompetitionDistance': ['CompDist', 'int'], \
'CompetitionOpenSinceMonth': ['CompOpenMonth', 'int'], 'CompetitionOpenSinceYear': ['CompOpenYear', 'int'], 'Promo2': ['Promo2', 'text'], \
'Promo2SinceWeek': ['Promo2Week', 'text'], 'Promo2SinceYear': ['Promo2Year', 'text']}
FIELDS_STORES_MONTHS = {'Store': ['Store', 'int'], 'PromoInterval': ['Month', int]}
#amount of rows to INSERT by one query
BUFF_SIZE = 500
MONTHES = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sept': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}

def process_line_months(headers, row):
	sql_text = ''
	store_id = row[headers.index('Store')]
	months = row[headers.index('PromoInterval')]
	if months <> '':
		months = months.split(',')
		for month in months:
			month = month.strip()
			if month in MONTHES.keys():
				sql_text = sql_text + store_id + ', ' + str(MONTHES[month]) + '), ('
			else:
				print months, month
	return sql_text

def upload_general_data(db_name, table_name, path, field_stores, buff_size, plf = None):
	try:
		con = psycopg2.connect(database=db_name, user=POSTGRE_USR, password=POSTGRE_PWD, host=POSTGRE_HOST, port=POSTGRE_PORT)
		cur = con.cursor()
		with open(path) as f:
			datareader = csv.reader(f, delimiter = ',')
			headers = datareader.next()
			columns_add = []
			sql_text_header = 'INSERT INTO ' + table_name + ' ('
			i = 0
			for title in headers:
				if title in field_stores.keys():
					sql_text_header += field_stores[title][0] + ', '
					columns_add.append(title)
			sql_text_header = sql_text_header[:-2] + ') VALUES ('
			lines_done = 0
			sql_text = ''
			for row in datareader:
				if len(row) == len(headers):
					lines_done += 1
					if plf == None:
						for title in columns_add:
							indx = headers.index(title)
							if row[indx] <> '':
								if field_stores[title][1] == 'text':
									sql_text += '\'' + row[indx] + '\', '
								elif field_stores[title][1] == 'date':
									sql_text += '\'' + row[indx] + '\', '
								else:
									sql_text += row[indx] + ', '
							else:
								sql_text += 'NULL, '
						sql_text = sql_text[:-2] +'), ('
					elif plf == 'process_line_months':
						sql_text += process_line_months(headers, row)
					if lines_done % buff_size == 0:
						sql_text = sql_text[:-3]
						sql_text = sql_text_header + sql_text
						cur.execute(sql_text)
						con.commit()
						sql_text = ''
						#print lines_done
			if lines_done % buff_size <> 0:
				sql_text = sql_text[:-3]
				sql_text = sql_text_header + sql_text
				cur.execute(sql_text)
				con.commit()
	except:
		print sql_text
		print lines_done
		print row
		print "error: ", traceback.format_exc()

if __name__ == "__main__":
	try:
		conn = psycopg2.connect(database=POSTGRE_DB_NAME, user=POSTGRE_USR, password=POSTGRE_PWD, host=POSTGRE_HOST, port=POSTGRE_PORT)
		cur = conn.cursor()
		cur.execute('SELECT 1 FROM information_schema.tables WHERE table_schema = \'public\' AND table_name = \'stores\'')
		if cur.rowcount == 0:
			cur.execute('CREATE TABLE Stores (Store INTEGER PRIMARY KEY, Type VARCHAR(3), Assortment VARCHAR(3), CompDist INTEGER, CompOpenMonth INTEGER, \
				CompOpenYear INTEGER, Promo2 VARCHAR(3), Promo2Week INTEGER, Promo2Year INTEGER)')
			conn.commit()
		cur.execute('SELECT 1 FROM information_schema.tables WHERE table_schema = \'public\' AND table_name = \'traindata\'')
		if cur.rowcount == 0:
			cur.execute('''CREATE TABLE TrainData (Store INTEGER, DayRef date, Sales INTEGER, Customers INTEGER, Open VARCHAR(3), Promo VARCHAR(3), \
				StateHoliday VARCHAR(3), SchoolHoliday VARCHAR(3), Promo2 VARCHAR(3), year_ int, week_ int, \
				f1 REAL, f2 REAL, f3 REAL, f4 REAL, f5 REAL, f6 REAL, f7 REAL, \
				f8 REAL, f9 REAL, f10 REAL, f11 REAL, f12 REAL, f13 REAL, f14 REAL, f15 REAL, f16 REAL, CONSTRAINT store_dayref primary key (Store, DayRef))''')
			conn.commit()
		cur.execute('SELECT 1 FROM information_schema.tables WHERE table_schema = \'public\' AND table_name = \'storespromo\'')
		if cur.rowcount == 0:
			cur.execute('CREATE TABLE StoresPromo (Store INTEGER, Month INTEGER, CONSTRAINT store_month primary key (Store, Month))')
			conn.commit()
		cur.execute('SELECT 1 FROM information_schema.tables WHERE table_schema = \'public\' AND table_name = \'testdata\'')
		if cur.rowcount == 0:
			cur.execute('CREATE TABLE TestData (ID INTEGER PRIMARY KEY, Store INTEGER, DayRef date, Open VARCHAR(3), Promo VARCHAR(3), \
				StateHoliday VARCHAR(3), SchoolHoliday VARCHAR(3), year_ int, week_ int, \
                f1 REAL, f2 REAL, f3 REAL, f4 REAL, f5 REAL, f6 REAL, f7 REAL, \
                f8 REAL, f9 REAL, f10 REAL, f11 REAL, f12 REAL, f13 REAL, f14 REAL, f15 REAL, f16 REAL)')
			conn.commit()
		cur.execute('SELECT 1 FROM information_schema.tables WHERE table_schema = \'public\' AND table_name = \'salesweekly\'')
		if cur.rowcount == 0:
			cur.execute('''CREATE TABLE salesweekly (store int, year int, week int, sales REAL, CONSTRAINT sales_week primary key (store, year, week))''')
			conn.commit()
        cur.execute('SELECT 1 FROM information_schema.tables WHERE table_schema = \'public\' AND table_name = \'avgsalesweekly\'')
        if cur.rowcount == 0:
            cur.execute('''CREATE TABLE avgsalesweekly(asst VARCHAR(3), year_ INTEGER, week_ INTEGER, sales REAL, CONSTRAINT asw PRIMARY KEY(asst,year_,week_))''')
            conn.commit()
		print 'all tables created'

		#upload data into tables
		cur.execute('SELECT COUNT(*) FROM stores')
		if cur.rowcount == 0:
			upload_general_data(POSTGRE_DB_NAME, 'Stores', PATH_STORES, FIELDS_STORES, BUFF_SIZE)
			print 'stores data uploaded'
		cur.execute('SELECT COUNT(*) FROM storespromo')
		if cur.rowcount == 0:
			upload_general_data(POSTGRE_DB_NAME, 'StoresPromo', PATH_STORES, FIELDS_STORES_MONTHS, BUFF_SIZE, 'process_line_months')
		cur.execute('SELECT COUNT(*) FROM traindata')
		if cur.rowcount == 0:
			upload_general_data(POSTGRE_DB_NAME, 'TrainData', PATH_TEST, FIELDS_TEST, BUFF_SIZE)
			print 'train data uploaded'
			cur.execute('''UPDATE TrainData SET Promo2 = 1 FROM Stores INNER JOIN StoresPromo ON Stores.Store = StoresPromo.Store \
				WHERE TrainData.Store = Stores.Store AND date_part(\'MONTH\', TrainData.dayref) = StoresPromo.month \
				AND Stores.Promo2 = \'1\' \
				AND (date_part(\'YEAR\', TrainData.DayRef) > Stores.promo2year \
					OR (date_part(\'WEEK\', DayRef) >= Stores.promo2week AND date_part(\'YEAR\', TrainData.DayRef) = Stores.promo2year))''')
			conn.commit()
			cur.execute('''UPDATE TrainData SET Promo2 = 0 WHERE Promo2 IS NULL''')
			conn.commit()
			print 'promo2  updated'
			cur.execute('''UPDATE traindata SET week_ = (case when ceil(date_part('DOY', dayref) / 7) IN (52,53) AND date_part('WEEK', dayref) = 1 then 1 \
				else date_part('WEEK', dayref) end), year_ = (case when ceil(date_part('DOY', dayref) / 7) IN (52,53) AND date_part('WEEK', dayref) = 1 \
				then date_part('YEAR', dayref)+1 else date_part('YEAR', dayref) end)''')
			conn.commit()
			cur.execute('''INSERT INTO salesweekly SELECT store, year_, week_, AVG(Sales) FROM TrainData GROUP BY store, year_, week_''')
			conn.commit()
			cur.execute('''CREATE INDEX td_indx ON traindata (store, year_, week_)''')
			conn.commit()
            cur.execute('''INSERT INTO avgsalesweekly SELECT st.assortment, td.year_, td.week_, AVG(td.Sales) FROM TrainData td INNER JOIN Stores st ON td.store = st.store GROUP BY st.assortment, year_, week_''')
            conn.commit()
			print 'train week & year updated'
		cur.execute('SELECT COUNT(*) FROM testdata')
		if cur.rowcount == 0:
			upload_general_data(POSTGRE_DB_NAME, 'TestData', PATH_TEST, FIELDS_TEST, BUFF_SIZE)
			cur.execute('''UPDATE testdata SET week_ = (case when ceil(date_part('DOY', dayref) / 7) IN (52,53) AND date_part('WEEK', dayref) = 1 then 1 \
				else date_part('WEEK', dayref) end), year_ = (case when ceil(date_part('DOY', dayref) / 7) IN (52,53) AND date_part('WEEK', dayref) = 1 \
				then date_part('YEAR', dayref)+1 else date_part('YEAR', dayref) end)''')
			conn.commit()
		conn.close()
	except:
		print 'error', traceback.format_exc()

